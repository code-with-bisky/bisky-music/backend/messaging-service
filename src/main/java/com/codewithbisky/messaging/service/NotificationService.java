package com.codewithbisky.messaging.service;

import com.codewithbisky.messaging.model.Notification;

public interface NotificationService {

    Notification send(Notification notification);
}
