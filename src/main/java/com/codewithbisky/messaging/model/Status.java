package com.codewithbisky.messaging.model;

public enum Status {
    PENDING,
    SENT,
    IN_PROGRESS,
    FAILED
}
