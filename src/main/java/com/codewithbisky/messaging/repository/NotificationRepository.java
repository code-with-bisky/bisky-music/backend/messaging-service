package com.codewithbisky.messaging.repository;

import com.codewithbisky.messaging.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification,String> {
}
