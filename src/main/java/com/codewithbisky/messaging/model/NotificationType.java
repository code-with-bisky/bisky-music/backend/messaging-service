package com.codewithbisky.messaging.model;

public enum NotificationType {
    EMAIL,
    SMS,
    PUSH_NOTIFICATION
}
