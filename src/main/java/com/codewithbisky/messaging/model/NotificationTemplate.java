package com.codewithbisky.messaging.model;

public enum NotificationTemplate {
    NEW_SONG_COMMENT,
    NEW_FOLLOWER
}
